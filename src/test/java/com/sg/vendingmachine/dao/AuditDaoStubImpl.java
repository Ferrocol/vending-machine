package com.sg.vendingmachine.dao;

import com.sg.vendingmachine.exceptions.FilePersistenceException;

public class AuditDaoStubImpl implements AuditDao {
    @Override
    public void writeAuditEntry(String entry) throws FilePersistenceException {}
}
