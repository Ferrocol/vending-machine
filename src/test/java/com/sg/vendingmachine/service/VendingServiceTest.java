package com.sg.vendingmachine.service;

import com.sg.vendingmachine.dto.Funds;
import com.sg.vendingmachine.dto.Item;
import com.sg.vendingmachine.exceptions.*;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.Map;

import static org.junit.Assert.*;

public class VendingServiceTest {

    private VendingService service;

    public VendingServiceTest() {
        ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
        service = ctx.getBean("service", VendingService.class);
    }

    @Test (expected = InvalidAmountAddedException.class)
    public void testAddFundsNull() throws Exception {
        service.addFunds(null);
    }

    @Test (expected = InvalidAmountAddedException.class)
    public void testAddFundsEmptyString() throws Exception {
        service.addFunds("");
    }

    @Test (expected = InvalidAmountAddedException.class)
    public void testAddFundsNotNumber() throws Exception {
        service.addFunds("string");
    }

    @Test
    public void testGetAvailableItemsReturnsAvailableItem() throws Exception {
        Map<String, Item> availableItems = service.getAvailableItems();
        assertTrue(availableItems.containsKey("T1"));
    }

    @Test
    public void testGetAvailableItemsDoesNotReturnUnavailableItem() throws Exception {
        Map<String, Item> availableItems = service.getAvailableItems();
        assertFalse(availableItems.containsKey("T0"));
    }

    @Test
    public void testPurchaseAvailableItemSufficientFunds() throws Exception {
        service.getAvailableItems();
        Item item = service.purchaseItem("T1", new Funds("1000"));
        assertTrue(item.getName().equals("Chips"));
    }

    @Test (expected = NoItemInventoryException.class) // exception expected because the item is not in our filtered map
    public void testPurchaseUnavailableItemSufficientFunds() throws Exception {
        service.getAvailableItems();
        service.purchaseItem("T0", new Funds("1000"));
    }

    @Test (expected = InsufficientFundsException.class)
    public void testPurchaseAvailableItemInsufficientFunds() throws Exception {
        service.getAvailableItems();
        service.purchaseItem("T1", new Funds("0"));
    }

    @Test (expected = NoItemInventoryException.class)
    public void testPurchaseUnavailableItemInsufficientFunds() throws Exception {
        service.getAvailableItems();
        service.purchaseItem("T0", new Funds("0"));
    }

    @Test (expected = InsufficientFundsException.class)
    public void testPurchaseAvailableItemNullFunds() throws Exception {
        service.getAvailableItems();
        service.purchaseItem("T1", null);
    }

    @Test (expected = NoItemInventoryException.class)
    public void testPurchaseUnavailableItemNullFunds() throws Exception {
        service.getAvailableItems();
        service.purchaseItem("T0", null);
    }

    @Test
    public void testChangeNeeded() {
        Funds oneCent = new Funds("0.01");
        assertTrue(service.isChangeNeeded(oneCent));
    }

    @Test
    public void testNullFundsChangeNotNeeded() {
        assertFalse(service.isChangeNeeded(null));
    }

    @Test
    public void testNoFundsChangeNotNeeded() {
        Funds noFunds = new Funds("0");
        assertFalse(service.isChangeNeeded(noFunds));
    }

    @Test (expected = NoFundsRemainException.class)
    public void testReturnNullChange() throws Exception {
        service.returnChange(null);
    }

    @Test (expected = NoFundsRemainException.class)
    public void testReturnNoChange() throws Exception {
        Funds zeroFunds = new Funds("0");
        service.returnChange(zeroFunds);
    }
}