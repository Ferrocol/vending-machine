package com.sg.vendingmachine.dao;

import com.sg.vendingmachine.dto.Item;
import com.sg.vendingmachine.exceptions.FilePersistenceException;

import java.util.Map;

public interface ItemDao {
    Map<String, Item> getAllItems() throws FilePersistenceException;
    Item dispenseItem(String itemSelection) throws FilePersistenceException;
}
