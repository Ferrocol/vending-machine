package com.sg.vendingmachine.dao;

import com.sg.vendingmachine.dto.Change;
import com.sg.vendingmachine.dto.Funds;

import java.math.BigDecimal;

public interface MoneyDao {
    Funds addFunds(String moneyInserted);
    Funds subtractFunds(BigDecimal itemPrice);
    Change returnChange(Funds funds);
}
