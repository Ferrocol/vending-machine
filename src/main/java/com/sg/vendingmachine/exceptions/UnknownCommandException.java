package com.sg.vendingmachine.exceptions;

public class UnknownCommandException extends Exception {

    public UnknownCommandException(String message) {
        super(message);
    }
}
