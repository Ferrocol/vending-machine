package com.sg.vendingmachine.exceptions;

public class FilePersistenceException extends Exception {

    public FilePersistenceException(String message, Throwable e) {
        super(message, e);
    }
}
