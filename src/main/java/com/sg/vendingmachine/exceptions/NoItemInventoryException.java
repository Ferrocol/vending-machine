package com.sg.vendingmachine.exceptions;

public class NoItemInventoryException extends Exception {

    public NoItemInventoryException(String message) {
        super(message);
    }
}
