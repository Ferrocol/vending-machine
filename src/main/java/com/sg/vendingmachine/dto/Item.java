package com.sg.vendingmachine.dto;

import java.math.BigDecimal;

public class Item {

    private String name;
    private BigDecimal price;
    private int quantity;

    public Item(String name, String priceString, String quantityString) {
        this.name = name;
        this.price = new BigDecimal(priceString);
        this.quantity = Integer.parseInt(quantityString);
    }

    public String getName() {
        return name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return " |Name: " + name + " |Price: $" + price + " |Qty Available: " + quantity;
    }
}
