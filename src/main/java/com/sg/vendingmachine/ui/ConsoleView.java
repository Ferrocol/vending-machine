package com.sg.vendingmachine.ui;

import com.sg.vendingmachine.dto.Change;
import com.sg.vendingmachine.dto.Funds;
import com.sg.vendingmachine.dto.Item;
import com.sg.vendingmachine.exceptions.UnknownCommandException;

import java.util.Map;

public class ConsoleView {
    private UserIO io;

    public ConsoleView(UserIO io) {
        this.io = io;
    }

    public void printMainMenu() {
        io.print("=== MAIN MENU ===");
        io.print("1. Display all items");
        io.print("2. Insert money");
        io.print("3. Purchase an item");
        io.print("4. Return change");
        io.print("0. Exit");
    }

    public int getUserMenuSelection() throws UnknownCommandException {
        int menuSelection;
        try {
            do {
                menuSelection = io.readInt("Please select from the menu options: ");
            } while (!isValidMenuSelection(menuSelection));
        } catch (NumberFormatException e) {
            throw new UnknownCommandException("ERROR: Unknown command!");
        }
        return menuSelection;
    }

    private boolean isValidMenuSelection(int menuSelection) {
        if (menuSelection < 0 || (menuSelection > 4 && menuSelection != 9999)) {
            io.print("Invalid input. Please enter a number between 0 and 4.");
            return false;
        } else {
            return true;
        }
    }

    public void printInsertMoneyBanner() {
        io.print("=== INSERT MONEY ===");
    }

    public String askUserToInsertMoney() {
        return io.readString("Enter dollar amount to be inserted: ");
    }

    public void printAvailableFunds(Funds funds) {
        io.print("Inserted: $" + funds.getAmount());
    }

    public void printAvailableItems(Map<String, Item> items) {
        for (String key : items.keySet()) {
            io.print(key + " | " + items.get(key).getName()
                    + " | $" + items.get(key).getPrice());
        }
    }

    public String getUserItemSelection() {
        return io.readString("Please enter item selection (e.g., B4):");
    }

    public void printPurchaseSuccessMessage(Item item) {
        io.print("Dispensing " + item.getName() + "...");
        io.print("Success!");
    }

    public boolean shouldReturnChange() {
        return io.readYesOrNo("Return change?");
    }

    public void printReturnChangeBanner() {
        io.print("=== RETURN CHANGE ===");
    }

    public void printChangeReturned(Change change) {
        io.print("Quarters: " + change.getQuarters());
        io.print("Dimes: " + change.getDimes());
        io.print("Nickels: " + change.getNickels());
        io.print("Pennies: " + change.getPennies());
    }

    public void userEnterToContinue() {
        io.readString("Press enter to continue.");
    }

    public void printExitMessage() {
        io.print("Goodbye!");
    }

    public void printErrorMessage(String errorMessage) {
        io.print(errorMessage);
    }
}
